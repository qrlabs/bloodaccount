# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-13 11:37
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_game'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='game_date',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
