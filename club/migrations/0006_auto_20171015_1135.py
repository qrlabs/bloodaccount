# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-15 10:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('club', '0005_club_last_sync'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='order',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Board Order'),
        ),
    ]
