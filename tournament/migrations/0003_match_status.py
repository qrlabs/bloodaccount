# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-15 10:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0002_auto_20171015_0804'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='status',
            field=models.PositiveIntegerField(choices=[(0, 'Pending'), (1, 'Started'), (2, 'Completed')], default=0),
        ),
    ]
