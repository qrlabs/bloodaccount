# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-10-15 06:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('club', '0003_member_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('white_score', models.PositiveIntegerField(default=0)),
                ('black_score', models.PositiveIntegerField(default=0)),
                ('comment', models.TextField(blank=True)),
                ('black', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_black', to='club.Member')),
            ],
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('started', models.DateTimeField()),
                ('ended', models.DateTimeField()),
                ('team_black', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='match_black', to='club.Club')),
                ('team_white', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='match_white', to='club.Club')),
            ],
        ),
        migrations.CreateModel(
            name='Tournament',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='TournamentRound',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=100)),
                ('tournament', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament.Tournament')),
            ],
        ),
        migrations.AddField(
            model_name='match',
            name='tournament_round',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament.TournamentRound'),
        ),
        migrations.AddField(
            model_name='game',
            name='match',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tournament.Match'),
        ),
        migrations.AddField(
            model_name='game',
            name='white',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='game_white', to='club.Member'),
        ),
    ]
